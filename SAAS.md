# SaaS Panels
Multiple clients' Admin/Store/Restaurant panels run on our [SaaS panel here](http://13.232.78.178:4447/auth/login).

For development and testing, there's a "Dev Settings" Option which lets you change between clients and their respective panels.
![Alt](https://d2wl0rug3f3g94.cloudfront.net/images/FKRG1616735118166-Ds3Q1616735117963Screenshot20210326at10.23.45AM.png)

![Alt](https://d2wl0rug3f3g94.cloudfront.net/images/CvTx1616736023100-qoyu1616736022974Screenshot20210326at10.48.16AM.png)

## Application Flow
At the time of writing there are 2 fundamental flows
* Large Inventory (Grocery, Pharmacy etc)
* Small Inventory (Restaurants, Liquor Stores etc)

Grocery, Pharmacy, Restaurants and the others are called "verticals". A vertical may fall under either large inventory flow or small inventory flow.

Verticles under large inventories will have the heirarchy - 

Admin -> Store -> Categories -> Sub-Categories -> Products

Verticles under small inventories will have the heirarchy - 
Admin ->  Restaurants (optional) -> Store -> Categories -> Menu Items + Add-Ons

There are 4 types of users and panels depending on the verticles the client is given.

* Admin
* Restaurant
* Store (large-inventory)
* Store (small-inventory)

Store and Restaurant users have limited functionality and have access to a subset of pages as compared to Admins

**Only admins have the ability two change verticles(if more than one verticles)**

* Admins are added through Role-Management
* Restaurant users are added by creating a restaurant. The email and password entered while adding can be used as credentials on the restaurant panel
* Adding store users is similar to restaurant users, store users are added by creating a store instead

You can change the panel from here
![Alt](https://d2wl0rug3f3g94.cloudfront.net/images/5I1X1616736181252-lRMp1616736181137Screenshot20210326at10.48.22AM.png)

### Testing
Under the hood, it's a single application but there still can be bugs specific to a client or a panel. Until we come up with an optimum method to test the SaaS panel, ALL panels have to be tested thoroughly (admin/restaurant/store)


