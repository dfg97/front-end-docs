# README : Front-End Deployment to EC2

### Getting Started

    This guide is not just specific to React apps, but can be used to deploy pretty much any front-end web app on an ec2 instance. All you have to do is generate your build (your final HTML, JS and CSS files) and put it on the server. The configurations remain the same regardless.

To generate a React App build, navigate to your project directory and run
`npm run build`

### Prerequisite:

You'll need a `private key (.pem)` file and `IP address` to `ssh` in the ec2 instance and deploy your front-end. Once aquired, the file must remain on your local machine and it's `permissions` set to `read-only` by the `root` user. To set the permissions, run
`sudo chmod 400 PATH_TO_PEM_FILE`
Once done, you can `ssh` to the server by running:
`ssh -i PATH_TO_PEM_FILE USER@IP_ADDRESS`
example:
`ssh -i ~/Downloads/oyegrocery_key.pem ubuntu@13.245.63.240`


## Installations

    The following must be installed on the server.

1. [Nginx](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-18-04): To check if already installed, run :
   `nginx -v` (returns nginx version if installed)
   To install, run :
   `sudo apt update`
   `sudo apt install nginx`
2. [Certbot](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx.html): To check if already installed, run :
   `sudo certbot --version` (duh)
   To install, run:  
   `sudo snap install core; sudo snap refresh core`  
   `sudo snap install --classic certbot`  
   And then verify with:  
   `sudo ln -s /snap/bin/certbot /usr/bin/certbot`
   `

### Firewall Setup

Use `ufw` to set up firewall rules. ufw comes pre-installed in ubuntu 18.04 and above, if not present, install by running
`sudo apt install ufw`  
Run the following commands to set up basic firewall rules  
`sudo ufw default deny incoming`  
`sudo ufw default allow outgoing`  
`sudo ufw allow ssh`  
`sudo ufw allow http`  
`sudo ufw allow https`  
If you want to allow ports:
`sudo ufw allow PORT_NUMBER/tcp`  
Then finally, run
`sudo ufw enable`  

### Nginx Configs

To verify nginx is up and running, run  
`sudo systemctl status nginx` (returns active/inactive)  
If inactive, you can start it by running  
`sudo systemctl start nginx`  

Your main config file is `nginx.conf` and is located in `/etc/nginx/`. However, it's best to keep the web-app's configurations separate for each project.

**Note: Before moving forward, it's best to have your build inside the `/var/www/` directory**

To move your build from your local machine to the server. You can

- Add and push the build folder in `git` by removing `/build` from your `.gitignore` file and commiting it with your code and then clone the repository inside `/var/www` and checkout the branch which contains the `build`. If redeploying, you can create and push the build again and run `git pull` in the cloned repo.
- Use `scp` or `rsync` commands to copy the `build` from your local machine to the server.
  [Copying files to the server over ssh using rsync](https://www.digitalocean.com/community/tutorials/how-to-copy-files-with-rsync-over-ssh)
  [Copying files to the server using scp](https://stackoverflow.com/questions/11388014/using-scp-to-copy-a-file-to-amazon-ec2-instance?rq=1)

#### Basic HTTP/HTTPS Configuration

After you have copied the build inside `/var/www` directory, do the following steps (Note: The following edits are made using `nano` editor, you can use `vim` or whatever you feel comfortable with)
Let's assume your `build` folder is present inside `/var/www/example`, and your web app's domain name is `my-website.com` and you want to host this app on port `443`, which is `HTTPS`. You'll be putting your own domain and build path instead of these.

1. Edit `nginx.conf` by running
   `sudo nano /etc/nginx/nginx.conf`
   And make sure the following line is present in the `http` block
   ` include /etc/nginx/mime.types;`
   Once done, hit `Ctrl+O` to write, then hit `Enter` to confirm. Then hit `Ctrl + X` to exit.

2. Create a file called `my-website.com` inside `/etc/nginx/sites-enabled` by using the touch command
   `sudo touch /etc/nginx/sites-enabled/my-website.com`
3. Open `my-website.com` file in the text editor and add these configs -

```
server{
	 listen 443;

	root /var/www/example/build;
	index index.html index.htm index.nginx-debian.html;

	server_name my-website.com www.my-website.com;

	location / {
		try_files $uri /index.html;
	}
}
```

4. Run `sudo nginx -t` to make sure your syntax is valid and then finally run  
   `sudo systemctl reload nginx`  
   **You'll have to do step 4 everytime there's a change in the nginx configuraitons**

And that's it, you should be able to see your web-app running at `https://IP_OF_THE_SERVER/`, although insecurely

### Enabling HTTPS and serving your web app using a domain

To do this, you'll need login to your domain provider's panel and add an `A Record` pointing to `your server's IP Address`. If you own `my-website.com`,
both `my-website.com` and `admin.my-website.com` are valid A-Records. Google how to create an `A-Record` as different provides may have diff

Once done with that, make sure the `server_name` values in the nginx config file you created earlier in `step 3` of `Basic HTTP/HTTPS configuration, match with the `A-records`.

At this point, visiting `https://my-webite.com` or whatever A-Record you created a
should run the `build` present in `/var/www/example/build`

But it will still say "Insecure Connection". To enable HTTPS, you'll need to generate ssl certificates by using certbot. It's just a single command  

`sudo certbot --nginx -d my-website.com`  

Now run `sudo systemctl reload nginx` for good measure and your web app should be using HTTPS!

#### Redirecting HTTP Traffic to HTTPS

Add the following lines in your `/etc/nginx/sites-enabled/example.com` configs

```
server {
    if ($host = my-website.com) {
        return 301 https://$host$request_uri;
    }
	listen 80 ;
	listen [::]:80 ;
    server_name my-website.com;
    return 404;
}
```

and run `sudo systemctl reload nginx`

That's it!
